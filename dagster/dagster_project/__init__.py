from dagster import Definitions
from . import resources
from . import allocateAPs
from . import startBuses

defs = Definitions(
    jobs=[startBuses.startBuses,
          allocateAPs.allocateAPs],
    resources=resources.resource_defs
)
