import traceback
import os
from typing import List, NoReturn

from dagster import op, graph, job, fs_io_manager
from dagster import execute_job, reconstructable
from dagster import Failure, failure_hook, HookContext, get_dagster_logger

from .params import input_params
from .resources import resource_defs
from . import workflowCreds

#
# See if the experiment needs to be started
#
@op(config_schema={"params" : dict},
    required_resource_keys={"portal"})
def maybeStartAPExperiment(context) -> bool:
    #context.log.info(str(status))
    params = context.op_config["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    result = portal.run(action="experiment.status",
                        name=params["ap_experiment"],
                        proj=params["proj"],
                        max_wait_time=5, errors_are_fatal=False,
                        wait_for_status=False,
                        context=context)
    #context.log.info(str(result))

    # Return False if the experiment is allready running
    if result["result_code"] == 0:
        return False

    #
    # Start the experiment and wait. 
    #
    result = portal.run(action="experiment.create",
                        name=params["ap_experiment"],
                        proj=params["proj"],
                        profile=params["ap_profile_name"],
                        paramset=params["ap_paramset"],
                        sshpubkey=workflowCreds.portalCreds["sshpubkey"],
                        max_wait_time=3600,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True,
                        context=context)
    context.log.debug(str(result))

    #
    # Do not want to raise an Exception here since, too big a hammer
    #
    if result["result_code"] != 0:
        return False

    return True

@job(
    config = {
        "ops" : {
            "maybeStartAPExperiment" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def allocateAPs():
    started = maybeStartAPExperiment()
    pass

