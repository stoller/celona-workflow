import tempfile

# Place holders
EMULAB_UID        = "leebee"
EMULAB_PID        = "PowderTeam"
GRAFANA_HOSTPORT  = "bar:3000"
GRAFANA_PASSWORD  = ""
INFLUXDB_HOSTPORT = "foo:8086"
INFLUXDB_PASSWORD = ""

# The workflow will create this import file so we have the actual user/project
try:
    from . import workflowDefs
    EMULAB_UID = workflowDefs.EMULAB_UID
    EMULAB_PID = workflowDefs.EMULAB_PID
    GRAFANA_HOSTPORT  = workflowDefs.GRAFANA_HOSTPORT
    GRAFANA_PASSWORD  = workflowDefs.GRAFANA_PASSWORD
    INFLUXDB_HOSTPORT = workflowDefs.INFLUXDB_HOSTPORT
    INFLUXDB_PASSWORD = workflowDefs.INFLUXDB_PASSWORD
except ImportError:
    pass    

# These are the same input params as with Stackstorm.
input_params = {
    'proj' : EMULAB_PID,
    'username' : EMULAB_UID,
    'ap_profile_name' : "PowderTeam,cl-ap-shvlan",
    'ap_paramset' : "leebee,cl-ap-dense",
    'ap_experiment' : "cl-ap-dense",
    'bus_profile_name' : "testbed,celona-workflow",
    'bus_paramset' : None,
    'bus_bindings' : {
        "influxHost"      : INFLUXDB_HOSTPORT,
        "influxPassword"  : INFLUXDB_PASSWORD
    },
    'bus_experiment' : "cl-buses",
    'hold_experiments' : True,
}
