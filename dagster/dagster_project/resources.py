from dagster import resource
from dagster import fs_io_manager

import dagster_emulab as portal
import dagster_paramiko as support
from . import workflowCreds

@resource(config_schema={"creds": dict})
def emulabPortal(context):
    creds = context.resource_config["creds"]
    return portal.EmulabBaseAction(creds)

@resource(config_schema={"creds": dict})
def sshClient(context):
    creds = context.resource_config["creds"]
    return support.sshClient(creds)

#
# resource definitions for jobs.
#
resource_defs = {
    "io_manager": fs_io_manager,
    "portal"    : emulabPortal.configured({"creds" : workflowCreds.portalCreds}),
    "sshClient" : sshClient.configured({"creds" : workflowCreds.managerCreds})
}

