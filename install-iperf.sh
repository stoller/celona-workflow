#!/bin/sh
set -x

SCRIPTNAME=$0
TMPDIR="/var/tmp"

sudo apt-get update

sudo apt-get -y install --no-install-recommends pip daemon
if [ $? -ne 0 ]; then
    echo 'ERROR: install pip/daemonfailed'
    exit 1
fi

sudo pip install influxdb-client pyserial pytz
if [ $? -ne 0 ]; then
    echo 'ERROR: pip install install failed'
    exit 1
fi

#
# Latest version of iperf fixes the server side hang problem.
#
tar -C $TMPDIR -zxf /local/repository/iperf-3.15.tar.gz
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not unpack iperf3 tarfile'
    exit 1
fi
(cd $TMPDIR/iperf-3.15; ./configure; make)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not configure/build iperf3'
    exit 1
fi
(cd $TMPDIR/iperf-3.15; sudo make install)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not install iperf3'
    exit 1
fi

sudo touch /etc/.iperf-installed
